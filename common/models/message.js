var fs = require('fs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*mongoose.connect('mongodb://localhost:27017/testing_storeImg');
*/module.exports = function(Message) {
    Message.greet = function(msg, cb) {
        process.nextTick(function() {
            msg = msg || 'hello';
            cb(null, 'Sagar says ' + msg + ' to TrySquad');
        });
    };
    Message.storingImages = function(req, res) {
        var schema = new Schema({
            img: {
                data: Buffer,
                contentType: String
            }
        });
        var imgPath = 'https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/16114118_1024317084340597_8373520220431108922_n.jpg?oh=0dd7fd2468ecd1b525c2b8b66f9ec33b&oe=59563B23';
        var A = mongoose.model('A', schema);
        mongoose.connection.on('open', function() {
                console.error('mongo is open');
                A.remove(function(err) {
                        if (err) throw err;
                        console.error('removed old docs');
                        var a = new A;
                        a.img.data = fs.readFileSync(imgPath);
                        a.img.contentType = 'image/png';
                        a.save(function(err, a) {
                                if (err) throw err;
                                console.error('saved img to mongo');
                                return;
                        });
                });
        });
    };
    Message.gettingImages = function(req, res) {
        A.findById(a, function(err, doc) {
            if (err) return next(err);
            res.contentType(doc.img.contentType);
            res.send(doc.img.data);
        });
    };
    Message.remoteMethod('storingImages', {
        isStatic: true,
        accepts: [{
            arg: 'String',
            type: 'any',
            description: 'image url',
            required: false,
            http: {
                source: 'body'
            }
        }],
        returns: [{
            description: 'Successful response',
            type: 'Message',
            arg: 'data',
            root: true
        }],
        http: {
            verb: 'get',
            path: '/storingImages'
        },
        description: 'Successful response\n'
    });
};